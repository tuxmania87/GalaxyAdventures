<?php
include("head.php");
include("navlogged.php");
include("klassen.php");

?>
<h2>Patchnotes - v2.1</h2>
<ul>
	<li><b>-&nbsp;&nbsp;neues Questsystem eingef&uuml;hrt</b></li>
	<li><b>-&nbsp;&nbsp;5 Kolonistenlevel eingef�hrt. strikt gekoppelt mit neuem Questsystem</b></li>
	<li><b>-&nbsp;&nbsp;neues Navigationssystem: Systeme eingef�hrt</b></li>
	<li><b>-&nbsp;&nbsp;neues Navigationssystem: Monde ( noch funktionslos, sp�ter kolonisierbar ) eingef�hrt</b></li>
	<li><b>-&nbsp;&nbsp;neue Alarmstufe: gelb (defensiv), neben bekannten rot (aggresiv ) und gr�n ( neutral )</b></li>
	<li><b>-&nbsp;&nbsp;Alarmstufen: neue Bilder</b></li>
	<li><b>-&nbsp;&nbsp;Planeten: Tag / Nachtzyklus (12h)</b></li>
	<li><b>-&nbsp;&nbsp;Planeten: neue Bilder</b></li>
	<li><b>-&nbsp;&nbsp;Flotten: neue Funktion zum Energieverteilen</b></li>
	<li><b>-&nbsp;&nbsp;Flotten: neue Funktion: Erz einsaugen</b></li>
	<li><b>-&nbsp;&nbsp;Flotten: neue Funktion: Deuterium einsaugen</b></li>
	<li><b>-&nbsp;&nbsp;Logbucheintr�ge �berarbeitet</b></li>
	<li><b>-&nbsp;&nbsp;Einf�hren von Questgegenst�nden zum abschliessen bestimmer Quests</b></li>
	<li><b>-&nbsp;&nbsp;neue Schiffstypen: Flagschiff. Man kann nur ein Flagschiff pro Account besitzen. mehr Infos folgen</b></li>
	<li><b>-&nbsp;&nbsp;neue Schiffstypen: Zu Erringen als Questbelohungen, bei Versteigerungen sowie bei versteckten Questgebern</b></li>
	<li><b>-&nbsp;&nbsp;... und vieles mehr</b></li>
</ul>
<?php
include("foot.php");
?>
	