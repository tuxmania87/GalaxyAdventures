<?php
define ("MAXMEM", 32*1024*1024);  //--- memory limit (32M) ---

$abschnitt = $arg1;

function drawBorder(&$img, &$color, $thickness = 1)
{
    $x1 = 0;
    $y1 = 0;
    $x2 = ImageSX($img) - 1;
    $y2 = ImageSY($img) - 1;

    for($i = 0; $i < $thickness; $i++)
    {
        ImageRectangle($img, $x1++, $y1++, $x2--, $y2--, $color);
    }
} 

header('Content-Type: image/png');
include("klassen.php");

$startx = 100 + $arg1 % 5 * 20;
$starty = 100 + floor($arg1/5) * 20;


$img = imagecreatetruecolor(680*5, 680*5);

for($i=0;$i<5;$i++) {
    for($j=0;$j<5;$j++) {
        $it = $j*5 + $i;
        $piece = imagecreatefrompng("minimap/bild".$it.".png");
        // Draw border
        //var_dump($piece);
        
        imagecopy($img,$piece,$i*680,$j*680,0,0,680,680);
        
    }
}

imagepng($img);
imagedestroy($img);
?>
