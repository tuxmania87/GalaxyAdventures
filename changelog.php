<?php
include("head.php");
include("nav.php");
?>
<h3>Changelog vom 4.3.2008</h3>
<ul>
<li>neue Domain: http://galaxy-adventures.net/</li>
<li>neue Forumadresse: http://forum.galaxy-adventures.net/</li>
<li>Alarmstufe rot funktioniert nun auch im Orbit korrekt</li>
<li>Logbuch wurde eingef&uuml;hrt: Angriffe und Diebst&auml;hle werden geloggt</li>
<li>Bauzeiten von Schiffen werden angezeigt</li>
<li>Jeder neuregistrierte Siedler bekommt eine Sonde</li>
<li>Man kann nun unter Schiffen Energie transferieren</li>
<li>Symbole f&uuml;rs Beamen und Etransfer von GA1 &uuml;bernommen</li>
<li>Bauzeiten f&uuml;r Fabriken wurden verk&uuml;rzt</li>
<li>neue Forschung: Kriegstreiberei</li>
<li>In der Schiffs&uuml;bersicht werden Schiffs ID angezeigt</li>
</ul>
<?php
include("foot.php");
?>

